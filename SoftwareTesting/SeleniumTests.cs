﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SoftwareTesting.PageObjects;
using EC = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace SoftwareTesting
{
    class SeleniumTests
    {
        IWebDriver driver;
        WebDriverWait waiter;

        [Test]
        public void TestHomePage()
        {
            HomePage homePage = new HomePage(driver);
            homePage.GoToPage();
            Thread.Sleep(5000);
            if(homePage.LinkRegister != null)
            {
                Assert.Pass();
            } else
            {
                Assert.Fail();
            }
        }

        [Test]
        public void TestRegistrationPage()
        {
            HomePage homePage = new HomePage(driver);
            homePage.GoToPage();
            Thread.Sleep(2000);
            RegistrationPage registrationPage = homePage.ClickOnRegisterLink();
            Thread.Sleep(2000);
            if (registrationPage.ButtonRegister != null)
            {
                Assert.Pass();
            } else
            {
                Assert.Fail();
            }
        }

        [Test]
        public void TestRegistration()
        {
            HomePage homePage = new HomePage(driver);
            homePage.GoToPage();
            Thread.Sleep(2000);
            RegistrationPage registrationPage = homePage.ClickOnRegisterLink();
            Thread.Sleep(2000);
            registrationPage.FirstName.SendKeys("Dragana");
            registrationPage.LastName.SendKeys("Kovacevic");
            registrationPage.Email.SendKeys("dadica97@icloud.com");
            registrationPage.UserName.SendKeys("Dadica");
            registrationPage.Password.SendKeys("DK1997");
            registrationPage.ConfirmPassword.SendKeys("DK1997");
            registrationPage.ClickOnRegisterButton();
            Thread.Sleep(2000);

            if(registrationPage.AlertSuccessful != null)
            {
                Assert.Pass();
            } else
            {
                Assert.Fail();
            }
        }

        [Test]
        [TestCase("Dadica", "DK1997")]
        public void TestLogin(string username, string password)
        {
            HomePage homePage = new HomePage(driver);
            homePage.GoToPage();
            Thread.Sleep(2000);
            LoginPage loginPage = homePage.ClickOnLoginLink();
            Thread.Sleep(2000);
            loginPage.KorisnickoIme.SendKeys(username);
            loginPage.Lozinka.SendKeys(password);
            HomePage retrievePage = loginPage.ClickOnButtonUlogujSe();

            if (retrievePage.LinkViewCart != null)
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail();
            }
        }

        [Test]
        public void TestOrderHistory()
        {
            HomePage homePage = new HomePage(driver);
            homePage.GoToPage();
            Thread.Sleep(2000);
            LoginPage loginPage = homePage.ClickOnLoginLink();
            Thread.Sleep(2000);
            loginPage.KorisnickoIme.SendKeys("Dadica");
            loginPage.Lozinka.SendKeys("DK1997");
            HomePage retrievePage = loginPage.ClickOnButtonUlogujSe();
            if(retrievePage.LinkViewCart != null)
            {
                retrievePage.OrderQuantity.SelectByValue("2");
                Thread.Sleep(2000);
                ViewPage viewPage = retrievePage.ClickOnLinkOrder();
                HomePage retrievePage2 = viewPage.ClickOnButtonContinue();
                retrievePage2.OrderQuantity.SelectByValue("3");
                Thread.Sleep(2000);
                ViewPage viewPage2 = retrievePage2.ClickOnLinkOrder();
                if(viewPage2 != null)
                {
                    CheckoutPage checkoutPage = viewPage2.ClickOnButtonCheckout();
                    HistoryPage historyPage = checkoutPage.ClickOnButtonHistory();

                    Assert.AreEqual(4000.0, historyPage.TotalSum);
                } else
                {
                    Assert.Fail();
                }

            } else 
            {
                Assert.Fail();
            }
        }

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            waiter = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
        }

        [TearDown]
        public void TearDown()
        {
            driver.Close();
        }
    }
}
