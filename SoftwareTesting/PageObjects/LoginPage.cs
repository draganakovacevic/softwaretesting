﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using EC = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace SoftwareTesting.PageObjects
{
    public class LoginPage
    {
        private readonly IWebDriver driver;
        private readonly WebDriverWait waiter;

        public LoginPage(IWebDriver driver)
        {
            this.driver = driver;
            waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
        }

        public IWebElement ButtonUlogujSe
        {
            get
            {
                IWebElement element = null;
                try
                {
                    waiter.Until(EC.ElementIsVisible(By.Name("login")));
                    element = driver.FindElement(By.Name("login"));
                }
                catch (Exception)
                {
                }
                return element;
            }
        }

        private IWebElement GetElement(By by)
        {
            IWebElement element;
            try
            {
                element = driver.FindElement(by);
            }
            catch (Exception)
            {
                element = null;
            }
            return element;
        }

        public IWebElement KorisnickoIme
        {
            get
            {
                return GetElement(By.Name("username"));
            }
        }

        public IWebElement Lozinka
        {
            get
            {
                return GetElement(By.Name("password"));
            }
        }

        public HomePage ClickOnButtonUlogujSe()
        {
            ButtonUlogujSe?.Click();
            waiter.Until(EC.ElementIsVisible(By.XPath("//a[@href='/cart']")));
            return new HomePage(driver);
        }
    }
}
