﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using EC = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace SoftwareTesting.PageObjects
{
    public class CheckoutPage
    {
        private readonly IWebDriver driver;
        private readonly WebDriverWait waiter;

        public CheckoutPage(IWebDriver driver)
        {
            this.driver = driver;
            waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
        }

        public IWebElement ButtonHistory
        {
            get
            {
                IWebElement element = null;
                try
                {
                    waiter.Until(EC.ElementIsVisible(By.XPath("//a[contains(text(), 'history']/@href")));
                    element = driver.FindElement(By.XPath("//a[contains(text(), 'history']/@href"));
                }
                catch (Exception)
                {
                }
                return element;
            }
        }

        public HistoryPage ClickOnButtonHistory()
        {
            ButtonHistory?.Click();
            waiter.Until(EC.ElementIsVisible(By.XPath("//a[@href='/cart']")));
            return new HistoryPage(driver);
        }
    }
}
