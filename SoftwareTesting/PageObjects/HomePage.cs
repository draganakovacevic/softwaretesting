﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using EC = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace SoftwareTesting.PageObjects
{
    public class HomePage
    {
        private readonly IWebDriver driver;
        private readonly WebDriverWait waiter;

        public HomePage(IWebDriver driver)
        {
            this.driver = driver;
            waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
        }

        public void GoToPage()
        {
            driver.Navigate().GoToUrl("http://shop.qa.rs/");
        }

        public IWebElement LinkRegister
        {
            get
            {
                IWebElement element = null;
                try
                {
                    waiter.Until(EC.ElementIsVisible(By.XPath("//a[@href='/register']")));
                    element = driver.FindElement(By.XPath("//a[@href='/register']"));
                }
                catch (Exception)
                {
                }
                return element;
            }
        }

        public IWebElement LinkLogin
        {
            get
            {
                IWebElement element = null;
                try
                {
                    waiter.Until(EC.ElementIsVisible(By.XPath("//a[@href='/login']")));
                    element = driver.FindElement(By.XPath("//a[@href='/login']"));
                }
                catch (Exception)
                {
                }
                return element;
            }
        }

        public RegistrationPage ClickOnRegisterLink()
        {
            LinkRegister?.Click();
            waiter.Until(EC.ElementIsVisible(By.ClassName("nemari")));
            return new RegistrationPage(driver);
        }

        public LoginPage ClickOnLoginLink()
        {
            LinkLogin?.Click();
            waiter.Until(EC.ElementIsVisible(By.Name("login")));
            return new LoginPage(driver);
        }

        public IWebElement LinkViewCart
        {
            get
            {
                IWebElement element = null;
                try
                {
                    waiter.Until(EC.ElementIsVisible(By.XPath("//a[@href='/cart']")));
                    element = driver.FindElement(By.XPath("//a[@href='/cart']"));
                }
                catch (Exception)
                {
                }
                return element;
            }
        }

        private SelectElement GetSelect(By by)
        {
            IWebElement element;
            SelectElement select;
            try
            {
                waiter.Until(EC.ElementIsVisible(by));
                element = driver.FindElement(by);
                select = new SelectElement(element);
            }
            catch (Exception)
            {
                select = null;
            }
            return select;
        }

        public SelectElement OrderQuantity
        {
            get
            {
                return GetSelect(By.XPath("//div[@class='panel-warning']/.//div[@class='panel-body']/.//select[@name='quantity']"));
            }
        }

        public IWebElement LinkOrder
        {
            get
            {
                IWebElement element = null;
                try
                {
                    waiter.Until(EC.ElementIsVisible(By.XPath("//div[@class='panel-warning']/.//div[@class='panel-body']/.//input[@type='submit']")));
                    element = driver.FindElement(By.XPath("//div[@class='panel-warning']/.//div[@class='panel-body']/.//input[@type='submit']"));
                }
                catch (Exception)
                {
                }
                return element;
            }
        }

        public ViewPage ClickOnLinkOrder()
        {
            LinkOrder?.Click();
            waiter.Until(EC.ElementIsVisible(By.XPath("//a[contains(text(), 'Continue']/@href")));
            return new ViewPage(driver);
        }
    }
}
