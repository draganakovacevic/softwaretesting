﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using EC = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace SoftwareTesting.PageObjects
{
    public class ViewPage
    {
        private readonly IWebDriver driver;
        private readonly WebDriverWait waiter;

        public ViewPage(IWebDriver driver)
        {
            this.driver = driver;
            waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
        }

        public IWebElement ButtonContinue
        {
            get
            {
                IWebElement element = null;
                try
                {
                    waiter.Until(EC.ElementIsVisible(By.XPath("//a[contains(text(), 'Continue']/@href")));
                    element = driver.FindElement(By.XPath("//a[contains(text(), 'Continue']/@href"));
                }
                catch (Exception)
                {
                }
                return element;
            }
        }

        public HomePage ClickOnButtonContinue()
        {
            ButtonContinue?.Click();
            waiter.Until(EC.ElementIsVisible(By.XPath("//a[@href='/cart']")));
            return new HomePage(driver);
        }

        public IWebElement ButtonCheckout
        {
            get
            {
                IWebElement element = null;
                try
                {
                    waiter.Until(EC.ElementIsVisible(By.Name("checkout")));
                    element = driver.FindElement(By.Name("checkout"));
                }
                catch (Exception)
                {
                }
                return element;
            }
        }

        public CheckoutPage ClickOnButtonCheckout()
        {
            ButtonCheckout?.Click();
            waiter.Until(EC.ElementIsVisible(By.XPath("//a[contains(text(), 'Go']/@href")));
            return new CheckoutPage(driver);
        }
    }
}
