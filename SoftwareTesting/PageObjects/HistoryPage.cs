﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using EC = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace SoftwareTesting.PageObjects
{
    public class HistoryPage
    {
        private readonly IWebDriver driver;
        private readonly WebDriverWait waiter;

        public HistoryPage(IWebDriver driver)
        {
            this.driver = driver;
            waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
        }

        public IWebElement TotalSum
        {
            get
            {
                IWebElement element = null;
                try
                {
                    IList<IWebElement> tdElements = driver.FindElements(By.TagName("td"));
                    element = tdElements[2];
                }
                catch (Exception)
                {
                }
                return element;
            }
        }
    }
}
