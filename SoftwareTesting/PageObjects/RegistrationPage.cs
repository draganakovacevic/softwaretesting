﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using EC = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace SoftwareTesting.PageObjects
{
    public class RegistrationPage
    {
        private readonly IWebDriver driver;
        private readonly WebDriverWait waiter;

        public RegistrationPage(IWebDriver driver)
        {
            this.driver = driver;
            waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
        }

        public IWebElement ButtonRegister
        {
            get
            {
                IWebElement element = null;
                try
                {
                    waiter.Until(EC.ElementIsVisible(By.Name("register")));
                    element = driver.FindElement(By.Name("register"));
                }
                catch (Exception)
                {
                }
                return element;
            }
        }

        private IWebElement GetElement(By by)
        {
            IWebElement element;
            try
            {
                element = driver.FindElement(by);
            }
            catch (Exception)
            {
                element = null;
            }
            return element;
        }

        public IWebElement FirstName
        {
            get
            {
                return GetElement(By.Name("ime"));
            }
        }

        public IWebElement LastName
        {
            get
            {
                return GetElement(By.Name("prezime"));
            }
        }

        public IWebElement Email
        {
            get
            {
                return GetElement(By.Name("email"));
            }
        }

        public IWebElement UserName
        {
            get
            {
                return GetElement(By.Name("korisnicko"));
            }
        }

        public IWebElement Password
        {
            get
            {
                return GetElement(By.Name("lozinka"));
            }
        }

        public IWebElement ConfirmPassword
        {
            get
            {
                return GetElement(By.Name("lozinkaOpet"));
            }
        }

        public IWebElement AlertSuccessful
        {
            get
            {
                IWebElement element = null;
                try
                {
                    waiter.Until(EC.ElementIsVisible(By.ClassName("alert-success")));
                    element = driver.FindElement(By.ClassName("alert-success"));
                }
                catch (Exception)
                {
                }
                return element;
            }
        }

        public HomePage ClickOnRegisterButton()
        {
            ButtonRegister?.Click();
            waiter.Until(EC.ElementIsVisible(By.ClassName("alert-success")));
            return new HomePage(driver);
        }
    }
}
